from django.db import models

# Create your models here.
class Schedule(models.Model):
	tanggal = models.DateField()
	jam = models.TimeField(auto_now_add=False)
	nama_kegiatan = models.CharField(max_length=50)
	tempat = models.CharField(max_length=50)
	kategori = models.CharField(max_length=40)

	def __str__(self):
		return self.nama_kegiatan