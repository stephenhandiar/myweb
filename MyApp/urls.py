from django.urls import path
from . import views

app_name = 'MyApp'

urlpatterns = [
    path('', views.index, name='index'),
    path('About/', views.about, name='about'),
    path('ComingSoon/', views.comingsoon, name='comingsoon'),      
    path('schedule/create/',views.schedule_create,name ='schedule_create'),
    path('schedule/',views.schedule,name = 'schedule_list'),
    path('schedule/<int:pk>/', views.schedule_detail, name='schedule_detail'),
    path('delete-all', views.schedule_delete,name = 'schedule_delete'),
    path('delete-sel/<id>', views.schedule_delete_selected, name = 'delete_selected'),
]