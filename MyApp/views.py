from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse
from .models import Schedule
from .forms import Schedule_Form

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def comingsoon(request):
    return render(request, 'comingsoon.html')

def schedule_create(request):
    if request.method == "POST":
        form = Schedule_Form(request.POST)
        if form.is_valid():
            schedule = form.save(commit=False)
            schedule.save()
            return redirect('schedule_detail', pk=schedule.pk)
    else:
        form = Schedule_Form()
    return render(request, 'schedule.html', {'form': form, })

def schedule_detail(request,pk):
    schedule = Schedule.objects.get(pk=pk)
    return render(request, 'schedule_details.html',{'schedule': schedule,})

def schedule(request):
    schedules = Schedule.objects.all()
    return render(request,'schedule_list.html',{'schedules' : schedules,})

def schedule_delete(request):
    Schedule.objects.all().delete()
    return redirect(schedule)

def schedule_delete_selected(request,id):
    deleted = Schedule.objects.get(id=id)
    deleted.delete()
    return render(request, 'schedule_list.html')
