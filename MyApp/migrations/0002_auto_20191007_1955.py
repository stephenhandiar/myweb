# Generated by Django 2.2.6 on 2019-10-07 12:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MyApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField()),
                ('nama_kegiatan', models.CharField(max_length=50)),
                ('tempat', models.CharField(max_length=50)),
                ('kategori', models.CharField(max_length=40)),
            ],
        ),
        migrations.DeleteModel(
            name='jadwal',
        ),
    ]
