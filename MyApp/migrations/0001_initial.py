# Generated by Django 2.2.6 on 2019-10-07 10:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_acara', models.CharField(max_length=50)),
                ('lokasi', models.CharField(max_length=50)),
                ('tanggal', models.DateField()),
                ('jam', models.TimeField(auto_now=True)),
                ('kategori', models.CharField(max_length=50)),
            ],
        ),
    ]
