from django import forms
from .models import Schedule
from django.forms import widgets

class Schedule_Form (forms.ModelForm):

    class Meta:
        model = Schedule
        fields = ['nama_kegiatan','tanggal','jam','tempat','kategori']
        widgets = {
            'nama_kegiatan' : forms.Textarea(attrs={'cols': 10, 'rows': 2}),
            'tanggal': forms.DateInput(attrs={'type': 'date'}),
            'jam': forms.TimeInput(attrs={'type': 'time'})}

    def __init__(self, *args, **kwargs):
        super(Schedule_Form,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })